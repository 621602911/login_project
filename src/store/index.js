import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  // มีไว้เปลี่ยน state เท่านั้น
  mutations: {

  },
  actions: {

  },
  modules: {
    auth
  }
})
